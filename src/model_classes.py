from src.img_helper import decode_class_names
def get_model_n_class_list(args):
    # Initializing the model specific processing and files
    model = args["model"]
    if model == "yolov3":
        class_name_path = "model_data/class_file.name"
    elif model == "tiny_yolov3":
        class_name_path = "model_data/coco_classes.txt"

    names = decode_class_names(class_name_path)

    return model, names